﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

	public float speed;
	public TextMeshProUGUI countText;
	public GameObject winTextObject;
	public GameObject loseTextObject;
	public GameObject eastWall;
	public GameObject ThirdPlatform;

	private float movementX;
	private float movementY;

	private Rigidbody rb;
	private int count;

	private bool destroyed = false;

	void Start()
	{
		ThirdPlatform.SetActive(false);

		rb = GetComponent<Rigidbody>();

		count = 0;

		UpdateBorders();
		winTextObject.SetActive(false);
	}

	void FixedUpdate()
	{
		Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        if (hasPlayerFallen())
        {
			loseTextObject.SetActive(true);
            if (destroyed == false)
            {
				Destroy(rb);
				destroyed = true;

				StartCoroutine(resetTimer());
			}
        } else
        {
			rb.AddForce(movement * speed);
			loseTextObject.SetActive(false);
        }
	}

	IEnumerator resetTimer()
    {
		yield return new WaitForSeconds(5);

		resetGame();
    }

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);

			count = count + 1;

			UpdateBorders();
		}
	}

	void OnMove(InputValue movementValue)
	{
		Vector2 movementVector = movementValue.Get<Vector2>();

		movementX = movementVector.x;
		movementY = movementVector.y;
	}

	void UpdateBorders()
	{
		countText.text = "Count: " + count.ToString();

		//first wall down
		if (count >= 12)
		{
			eastWall.SetActive(false);
		}

		//second area opened
		if (count >= 16)
		{
			ThirdPlatform.SetActive(true);
		}

		//win
		if (count >= 20)
		{
			winTextObject.SetActive(true);
		}
	}

	bool hasPlayerFallen()
    {
		if(destroyed == false)
        {
			if (rb.transform.position.y < -5)
			{
				//Debug.Log("fell");
				return true;
			}
			else
			{
				return false;
			}
		}
		return true;
    }

	void resetGame()
    {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}